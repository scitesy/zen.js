import {Wallet} from  '../src/Wallet'

describe('wallet', () => {
    it('connect to remote node', async () => {
        const wallet = Wallet.fromMnemonic('feel muffin volcano click mercy abuse bachelor ginger limb tomorrow okay input spend athlete boring security document exclude liar dune usage camera ranch thought', 'test',
            new Wallet.RemoteNodeWalletActions('http://localhost:3000'))

        await wallet.refresh()
        console.log(wallet.getAddress())
        console.log(wallet.getBalance())

        await wallet.send([{
            address:'tzn1qxp6ekp72q8903efylsnej34pa940cd2xae03l49pe7hkg3mrc26qyh2rgr',
            asset:'00',
            amount:100
        }])
    })
})