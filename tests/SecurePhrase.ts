import 'mocha'
import { expect } from 'chai'

import SecuraPhrase from '../src/SecurePhrase'

describe('SecurePhrase', () => {
    it('can enrypt and decrypt', () => {
        const plain = 'hello world'
        const password = '1234'

        const cipher = SecuraPhrase.encrypt(password, plain)

        const msg = SecuraPhrase.decrypt(password, cipher)

        console.log(msg)

        expect(msg).to.equal(plain)
    })
})