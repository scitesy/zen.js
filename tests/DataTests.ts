import {fromYaml,toYaml, serialize} from '../src/Data'
import { expect } from 'chai'

describe('Zen.Data', () => {
    it('Can parse yaml into data', () => {

        const yaml = 'text: hello\nreturnAddress: zen1q03jc77dtd2x2gk90f40p9ezv5pf3e2wm5hy8me2xuxzmjneachrq6g05w5\namount: 1.1ZP\nuint64: 111UL\nint64: 5'

        const data = fromYaml('main',yaml)

        expect(serialize(data)).to.be.equal('0c050474657874060568656c6c6f05696e74363401000000000000000506616d6f756e740500000000068e77800675696e74363405000000000000006f0d72657475726e416464726573730802207c658f79ab6a8ca458af4d5e12e44ca0531ca9dba5c87de546e185b94f3dc5c6')
    })

    it('Parse 50zp correctly', () => {
        const yaml = '50ZP'
        const data = fromYaml('main', yaml)

        console.log(toYaml('main',data))
        console.log(serialize(data))
    })
})