import {writeAmount,readAmount, getSizeOfAmount} from '../src/Serialization'
import { expect } from 'chai'
import BigInteger = require('bigi')
import 'mocha'
import {Asset} from '../src/Types'

describe('Amount serialization', () => {
    it('amount values less than 1E4 round trip', () => {
        for (let i = 0; i < 10000; i++) {
            const big = new BigInteger(i.toString(),undefined,undefined)

            const buffer = Buffer.alloc(getSizeOfAmount(big))

            writeAmount(big, buffer,0)
            const {amount:res} = readAmount(buffer, 0)

            expect(res.equals(big)).to.be.true
        }
    })

    it('FFFFFFFFFFFFFFFF', () => {
        const big = BigInteger.fromHex('FFFFFFFFFFFFFFFF')
        const buffer = Buffer.alloc(getSizeOfAmount(big))
        writeAmount(big, buffer,0)
        const {amount:res} = readAmount(buffer, 0)

        expect(res.equals(big)).to.be.true
    })

    it('FFFFFFFFFFFFFF00', () => {
        const big = BigInteger.fromHex('FFFFFFFFFFFFFF00')
        const buffer = Buffer.alloc(getSizeOfAmount(big))
        writeAmount(big, buffer,0)
        const {amount:res} = readAmount(buffer, 0)

        expect(res.equals(big)).to.be.true
    })

    it('0000000FFFFFFF00', () => {
        const big = BigInteger.fromHex('0000000FFFFFFF00')
        const buffer = Buffer.alloc(getSizeOfAmount(big))
        writeAmount(big, buffer,0)
        const {amount:res} = readAmount(buffer, 0)

        expect(res.equals(big)).to.be.true
    })

    it('0000000FFFF00001', () => {
        const big = BigInteger.fromHex('0000000FFFF00001')
        const buffer = Buffer.alloc(getSizeOfAmount(big))
        writeAmount(big, buffer,0)
        const {amount:res} = readAmount(buffer, 0)

        expect(res.equals(big)).to.be.true
    })
})

describe('Asset serialization', () => {
    function checkAsset(name:string) {
        const asset = new Asset(name)

        const size = asset.getSize()

        const buffer = Buffer.alloc(size)

        const offset = asset.write(buffer,0)

        expect(offset).to.be.equals(size)

        const {asset:asset2,offset:offset2} = Asset.read(buffer, 0)

        expect(asset2.asset).to.be.equals(name)
        expect(offset2).to.be.equals(size)
    }

    it('serialize and deserialize Zen Asset', () => checkAsset('00'))
    it('serialize and deserialize cHash only', () => checkAsset('000000001111111111111111111111111111111111111111111111111111111111111111'))
    it('serialize and deserialize compressable Subtype', () =>
        checkAsset('000000001111111111111111111111111111111111111111111111111111111111111111' +
                                 '2222222222222222000000000000000000000000000000000000000000000000'))
    it('serialize and deserialize uncompressable Subtype', () =>
        checkAsset('000000001111111111111111111111111111111111111111111111111111111111111111' +
                                 '2222222222222222222222222222222222222222222222222222222222222200'))
})