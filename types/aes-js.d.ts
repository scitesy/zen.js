
declare module 'aes-js' {

    namespace aesjs {
        namespace ModeOfOperation {
            class cbc {
                constructor(key:Buffer,iv:Buffer)
                encrypt(plain:Buffer) : Uint8Array
                decrypt(cipher:Buffer) : Uint8Array
            }
        }
    }

    export = aesjs
}