declare module 'pkcs7' {
    export function pad(plaintext:Buffer):Buffer
    export function unpad(padded:Buffer):Buffer
}