declare module 'create-hmac' {
    class hmac {
        update(data:Buffer): void
        digest():Buffer
    }

    function createHmac(alg:string, secretKey:Buffer) : hmac;

    export = createHmac
}