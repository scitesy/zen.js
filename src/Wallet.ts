import {reduce,has, get, set, forIn } from 'lodash'
import axios from 'axios'
import {ExtendedKey, PrivateKey} from './Crypto'
import Address from './Address'
import {Chain} from './Types'
import {TransactionBuilder} from './TransactionBuilder'

export class Wallet {
    private unspentOutputs: Wallet.PointedOutputs
    private externalPKHash: string
    private changePKHash: string

    constructor(private zenKey: ExtendedKey, private chain: Chain, private actions: Wallet.WalletActions) {
        this.externalPKHash = zenKey.derive(0).derive(0).getPublicKey().hash().hash
        this.changePKHash = zenKey.derive(1).derive(0).getPublicKey().hash().hash
        this.unspentOutputs = []
    }

    static fromMnemonic(phrase: string, chain: Chain, actions: Wallet.WalletActions) {
        const key = ExtendedKey.fromMnemonic(phrase)
        const zenKey = key.derivePath("m/44'/258'/0'")

        return new Wallet(zenKey, chain, actions)
    }

    async refresh() {
        // TODO: we always ask for all unspent outputs, we might able to make it more efficient by requesting deltas only

        this.unspentOutputs = await this.actions.getUnspentOutputs([
            Address.getPublicKeyHashAddress(this.chain, this.externalPKHash),
            Address.getPublicKeyHashAddress(this.chain, this.changePKHash)
        ])
    }

    getBalance() {
        return reduce(this.unspentOutputs, (balance, pointedOutput) => {
            const {asset, amount} = pointedOutput.spend

            if (has(balance, asset)) {
                const accumulatedAmount = get(balance, asset)
                return set(balance, asset, accumulatedAmount + amount)
            } else {
                return set(balance, asset, amount)
            }
        }, {})
    }

    getAddress() {
        return Address.getPublicKeyHashAddress(this.chain, this.externalPKHash)
    }

    private collectInputs(requiredAmounts: { [s: string]: number }) {
        const changePrivateKey = this.zenKey.derive(1).derive(0).getPrivateKey()
        const externalPrivateKey = this.zenKey.derive(0).derive(0).getPrivateKey()

        const inputs: Array<[PrivateKey, Wallet.Outpoint]> = []
        const change: { [s: string]: number } = {}

        forIn(requiredAmounts, (amount, asset) => {
            let collectedAmount = 0

            for (let output of this.unspentOutputs) {
                // TODO: we only support spending pklock, coinbase is not yet supported
                const pkLock = output.lock.PK

                if (pkLock && output.spend.asset === asset) {
                    collectedAmount += output.spend.amount

                    let privateKey: PrivateKey

                    if (pkLock.hash === this.externalPKHash) {
                        privateKey = externalPrivateKey
                    }
                    else {
                        privateKey = changePrivateKey
                    }

                    inputs.push([privateKey, output.outpoint])

                    if (collectedAmount === amount) {
                        break
                    } else if (collectedAmount > amount) {
                        set(change, asset, collectedAmount - amount)
                    }
                }
            }

            if (collectedAmount < amount) {
                throw 'not enough tokens'
            }
        })

        return {inputs, change}
    }

    private getChangeAddress() {
        return Address.getPublicKeyHashAddress(this.chain, this.changePKHash)
    }

    async send(outputs: Array<{ address: string, asset: string, amount: number }>) {
        const requiredAmounts = reduce(outputs, (amounts, output) => {
            if (has(amounts, output.asset)) {
                const amount = get(amounts, output.asset)
                return set(amounts, output.asset, amount + output.amount)
            } else {
                return set(amounts, output.asset, output.amount)
            }
        }, {})

        const {inputs, change} = this.collectInputs(requiredAmounts)

        const txBuilder = new TransactionBuilder(this.chain)
        inputs.forEach(([privateKey, input]) => txBuilder.addInput(input.txHash, input.index, privateKey))
        outputs.forEach(output => txBuilder.addOutput(output.address, output.amount, output.asset))

        const changeAddress = this.getChangeAddress()

        forIn(change, (amount, asset) => txBuilder.addOutput(changeAddress, amount, asset))

        const tx = txBuilder.sign().toHex()

        return await this.actions.publish(tx)
    }
}

export namespace Wallet {
    export interface Outpoint {
        txHash: string,
        index: number
    }

    export type PointedOutput = {
        outpoint:Outpoint,
        spend: {
            asset: string,
            amount: number
        },
        lock: {
            PK?: {
                hash: string,
                address: string
            }
        }
    }
    export type PointedOutputs = Array<PointedOutput>

    export interface WalletActions {
        getUnspentOutputs: (addresses: Array<string>) => Promise<PointedOutputs>
        publish: (tx: string) => Promise<string>
    }

    export class RemoteNodeWalletActions implements WalletActions {
        constructor(public url: string) {

        }

        async getUnspentOutputs(addresses: Array<string>) {
            const response = await axios.post<PointedOutputs>(`${this.url}/api/unspentoutputs`, addresses)

            if (response.status === 200)
                return response.data
            else
                throw response.statusText
        }

        async publish(tx: string) {
            const response = await axios.post<string>(`${this.url}/api/publishtransaction`, {tx: tx})

            if (response.status === 200)
                return response.data
            else
                throw response.statusText
        }
    }
}