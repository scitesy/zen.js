import {Buffer} from 'buffer'
import {sumBy, isUndefined} from 'lodash'
import {sha3_256} from 'js-sha3'
import {Input,Output,Contract, Witness, Hash} from './Types'
import {getSizeOfVarInt,writeVarInt,readVarInt} from './Serialization'

export class Transaction {
    witnesses:Array<Witness>

    constructor(
        public version:number,
        public inputs:Array<Input>,
        public outputs:Array<Output>,
        public contract?: Contract,
        witnesses?: Array<Witness>) {

        if (isUndefined(witnesses)) {
            this.witnesses = []
        } else {
            this.witnesses = witnesses
        }
    }

    getSize(full:boolean) {
        const inputsSize =
            getSizeOfVarInt(this.inputs.length) +
            sumBy(this.inputs, input => input.getSize())

        const outputsSize =
            getSizeOfVarInt(this.outputs.length) +
            sumBy(this.outputs, output => output.getSize())

        const witnessesSize =
            full ?
                getSizeOfVarInt(this.witnesses.length) +
                sumBy(this.witnesses, witness => witness.getSize())
                :
                0

        const contractSize =
            1 + (isUndefined(this.contract) ? 0 : this.contract.getSize())

        return 4 + inputsSize + outputsSize + witnessesSize + contractSize
    }

    write(buffer:Buffer,offset:number, full:boolean) {
        offset = buffer.writeUInt32BE(this.version, offset)

        offset = writeVarInt(this.inputs.length, buffer, offset)
        this.inputs.forEach(input => offset = input.write(buffer,offset))

        offset = writeVarInt(this.outputs.length, buffer,offset)
        this.outputs.forEach(output => offset = output.write(buffer,offset))

        if (isUndefined(this.contract)) {
            offset = buffer.writeUInt8(0, offset)
        } else {
            offset = buffer.writeUInt8(1, offset)
            offset = this.contract.write(buffer,offset)
        }

        if (full) {
            offset = writeVarInt(this.witnesses.length, buffer,offset)
            this.witnesses.forEach(witness => offset = witness.write(buffer,offset))
        }
    }

    static read(buffer:Buffer, offset:number, full:boolean) {
        const version = buffer.readUInt32BE(offset)
        offset += 4

        const {value:inputsLength, offset:tempOffset} = readVarInt(buffer,offset)
        offset = tempOffset

        const inputs:Array<Input> = []
        for (let i = 0; i < inputsLength; i++) {
            const {input,offset:inputOffset} = Input.read(buffer, offset)

            inputs.push(input)

            offset = inputOffset
        }

        const {value:outputsLength, offset:tempOffset2} = readVarInt(buffer,offset)
        offset = tempOffset2

        const outputs:Array<Output> = []
        for (let i = 0; i < outputsLength; i++) {
            const {output,offset:outputOffset} = Output.read(buffer, offset)

            outputs.push(output)

            offset = outputOffset
        }

        const isContract = buffer.readUInt8(offset)
        offset++

        let contract = undefined

        if (isContract === 1) {
            const {contract: innerContract, offset:tempOffset} = Contract.read(buffer,offset)

            offset = tempOffset
            contract = innerContract
        }

        let witnesses:Array<Witness> = []

        if (full) {
            const {value:witnessesLength, offset:tempOffset} = readVarInt(buffer,offset)
            offset = tempOffset

            for (let i = 0; i < witnessesLength; i++) {
                const {witness,offset:witnessOffset} = Witness.read(buffer, offset)

                witnesses.push(witness)

                offset = witnessOffset
            }
        }

        const tx = new Transaction(version, inputs, outputs, contract, witnesses)

        return {tx, offset}
    }

    serialize(full:boolean) {
        const size = this.getSize(full)
        const buffer = Buffer.alloc(size)

        this.write(buffer, 0, full)

        return buffer
    }

    toHex(full:boolean = true) {
        return this.serialize(full).toString('hex')
    }

    static fromHex(hex:string, full:boolean = true) {
        const buffer = Buffer.from(hex,'hex')

        const {tx} = Transaction.read(buffer,0, full)

        return tx
    }

    toJson() {
        return {
            version:this.version,
            inputs: this.inputs.map(input => input.toJson()),
            outputs: this.outputs.map(input => input.toJson()),
            witnesses: this.witnesses.map(input => input.toJson()),
            contract: this.contract ? this.contract.toJson() : undefined
        }
    }

    hash() {
        const buffer = this.serialize(false)
        return new Hash(sha3_256(buffer))
    }
}

