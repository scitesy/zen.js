import {HDNode, ECPair, ECSignature, networks} from 'bitcoinjs-lib'
import * as bech32 from 'bech32'
import {sha3_256} from 'js-sha3'
import Mnemonic from './Mnemonic'
import Address from './Address'
import {Hash, Chain} from './Types'

export class Signature {

    constructor(public _signature:ECSignature) {

    }

    serialize() {
        const signarueBuffer = Buffer.alloc(64)
        this._signature.toCompact(0,true).copy(signarueBuffer,0,1)

        return signarueBuffer
    }

    static deserialize(signarueBuffer:Buffer) {
        const buffer = Buffer.alloc(65)
        signarueBuffer.copy(buffer,1)

        buffer.writeInt8(31, 0)

        const {signature} = ECSignature.parseCompact(buffer)

        return new Signature(signature)
    }
}

export class PublicKey {
    private publicKey:ECPair

    private constructor(publicKeyBuffer:Buffer) {
        // TODO:implement our own network
        this.publicKey = ECPair.fromPublicKeyBuffer(publicKeyBuffer, networks.testnet)
    }

    verify(msg:Buffer, signature:Signature) {
        return this.publicKey.verify(msg, signature._signature)
    }

    serialize() {
        return this.publicKey.getPublicKeyBuffer()
    }

    static deserialize(buffer:Buffer) {
        return new PublicKey(buffer)
    }

    hash() {
        const buffer = this.serialize()
        return new Hash(sha3_256(buffer))
    }

    toAddress(chain:Chain) {
        return Address.getPublicKeyHashAddress(chain, this.hash())
    }
}

export class PrivateKey {
    constructor(private pair:ECPair) {
        if (!pair.compressed) {
            throw 'only compressed key is supported'
        }
    }

    sign(msg:Buffer) {
        return new Signature(this.pair.sign(msg))
    }

    getPublicKey() {
        return PublicKey.deserialize(this.pair.getPublicKeyBuffer())
    }
}

export class ExtendedKey {
    private constructor(private node:HDNode) {

    }

    static fromMnemonic(mnemonic:string) {
        const seed = Mnemonic.mnemonicToSeed(mnemonic)

        // TODO: create zen network
        return new ExtendedKey(HDNode.fromSeedBuffer(seed))
    }

    derive(index:number) {
        return new ExtendedKey(this.node.derive(index))
    }

    deriveHardened(index:number){
        return new ExtendedKey(this.node.deriveHardened(index))
    }

    derivePath(path:string) {
        return new ExtendedKey(this.node.derivePath(path))
    }

    neutered() {
        return new ExtendedKey(this.node.neutered())
    }

    getPrivateKey() {
        if (!this.node.keyPair.d) {
            throw 'no private key'
        }

        return new PrivateKey(this.node.keyPair)
    }

    getPublicKey() {
        return PublicKey.deserialize(this.node.getPublicKeyBuffer())
    }

    toAddress(chain:Chain) {
        return this.getPublicKey().toAddress(chain)
    }

    toBase58() {
        return this.node.toBase58()
    }

    static fromBase58(string:string) {
        return HDNode.fromBase58(string)
    }
}