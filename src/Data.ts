import {Type,Schema, load,dump} from 'js-yaml'
import Address from './Address'
import {Chain} from './Types'
import BigInteger = require('bigi')
import {endsWith,replace, isInteger,toPairs,orderBy,isPlainObject,sumBy} from 'lodash'
import {Buffer} from "buffer"
import {getSizeOfVarInt,writeVarInt} from './Serialization'

export namespace Data {
    export class Dictionary {
        constructor(public data:Array<[string, any]>) {

        }

        getSize() {
            const total = sumBy(this.data, pair => {
                return getSizeOfVarInt(pair[0].length) + pair[0].length + pair[1].getSize()
            })

            return total + getSizeOfVarInt(this.data.length) + 1
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(12, offset)
            offset = writeVarInt(this.data.length, buffer, offset)

            const pairs = this.data.sort( (x,y) => {
                const key1 = x[0]
                const key2 = y[0]

                if (key1.length < key2.length) {
                    return -1
                } else if (key1.length > key2.length) {
                    return 1
                } else {
                    for (let i = 0;i< key1.length; i++) {
                        const code1 = key1.charCodeAt(i)
                        const code2 = key2.charCodeAt(i)

                        if (code1 < code2)
                            return -1;
                        else if (code1 > code2)
                            return 1;
                    }

                    return 0
                }
            })

            pairs.forEach(pair => {
                offset = writeVarInt(pair[0].length,buffer,offset)
                offset += buffer.write(pair[0], offset, pair[0].length, 'ascii')
                offset = pair[1].write(buffer, offset)
            })

            return offset
        }
    }

    export class PKAddress {
        constructor(public hash:Buffer) {

        }

        getSize() {
            return 1 + getSizeOfVarInt(2) + getSizeOfVarInt(32) + 32
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(8,offset)
            offset = writeVarInt(2, buffer, offset)
            offset = writeVarInt(32, buffer,offset)
            this.hash.copy(buffer, offset)

            return offset + 32
        }
    }

    export class UInt64 {
        constructor(public value:BigInteger) {
        }

        getSize() {
            return 9
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(5,offset)

            this.value.toBuffer(8).copy(buffer, offset)

            return offset + 8
        }
    }

    export class Int64 {
        constructor(public value:BigInteger) {
        }

        getSize() {
            return 9
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(1,offset)
            this.value.toBuffer(8).copy(buffer, offset)

            return offset + 8
        }
    }

    export class String {
        constructor(public s:string) {

        }

        getSize() {
            return this.s.length + 1 + getSizeOfVarInt(this.s.length)
        }

        write(buffer:Buffer,offset:number) {
            offset = buffer.writeUInt8(6,offset)
            offset = writeVarInt(this.s.length, buffer, offset)
            buffer.write(this.s,offset,this.s.length, 'ascii')

            return offset + this.s.length
        }
    }

    function isDecCode(c:number) {
        return ((0x30/* 0 */ <= c) && (c <= 0x39/* 9 */));
    }

    function isNumber(data:string) {
        for (let index = 0; index < data.length; index++) {
            if (!isDecCode(data.charCodeAt(index))){
                return false
            }
        }

        return true
    }

    const DictionaryYamlType = new Type('tag:yaml.org,2002:map', {
        kind: 'mapping',
        construct: function (data) {
            return new Dictionary(data !== null ? data : {})
        },
        instanceOf: Dictionary,
        represent:function (data:any) {
            const dict:Dictionary=data
            return dict.data
        }
    })

    const StringYamlType = new Type('tag:yaml.org,2002:str', {
        kind: 'scalar',
        construct: function (data) {
            return new String(data !== null ? data : '')
        },
        instanceOf:String,
        represent(data:any) {
            return data.s
        }
    })

    const Int64YamlType = new Type('tag:yaml.org,2002:int', {
        kind:'scalar',
        resolve(data) {
            if (typeof data === 'number') {
                return isInteger(data)
            }
            else if (typeof data === 'string') {
                if (data[0] === '-') {
                    data = data.substr(1)
                }

                return isNumber(data)
            }

            return false
        },
        construct: function (data) {
            if (typeof data === 'number') {
                return new Int64(new BigInteger(data.toString(),10,undefined))
            }
            else if (typeof data === 'string') {
                return new Int64(new BigInteger(data,10,undefined))
            }
        },
        instanceOf:Int64,
        represent: function (data:any) {
            const int64:Int64=data

            return int64.value.toString()
        }
    })

    const UInt64YamlType = new Type('!uint64', {
        kind:'scalar',
        resolve(data) {
            if (typeof data === 'string' && endsWith(data, 'UL')) {
                const amount = data.substr(0, data.length-2)

                return isNumber(amount)
            }

            return false
        },
        construct: function (data) {
            if (typeof data === 'string') {
                const amount = data.substr(0, data.length-2)

                return new UInt64(new BigInteger(amount, 10,undefined))
            }
        },
        instanceOf:UInt64,
        represent: function (data:any) {
            const uint64:UInt64=data

            return uint64.value.toString() + 'UL'
        }
    })

    const ZenAmountYamlType = new Type('!zen', {
        kind:'scalar',
        resolve: function (data) {
            if (typeof data === 'string' && endsWith(data, 'ZP')) {
                const amount = data.substr(0, data.length-2)

                if (isNumber(amount)) {
                    return true
                } else {
                    const decimalPoint = amount.indexOf('.')

                    if (decimalPoint === -1) {
                        return false
                    }

                    const x = amount.substr(0, decimalPoint)
                    const y = amount.substr(decimalPoint+1)

                    return isNumber(x) && isNumber(y)
                }
            }

            return false
        },

        construct: function (data) {
            // TODO:handle the case of number higher than javascript maximum

            const amount = data.substr(0, data.length-2).trim()

            const decimalPoint = amount.indexOf('.')

            let decimalLength = 0

            if (decimalPoint !== -1) {
                decimalLength = amount.length - decimalPoint - 1
            }

            let multiplier = Math.pow(10,8 - decimalLength)

            return new UInt64(new BigInteger(replace(amount, '.',''),10,undefined).multiply(BigInteger.valueOf(multiplier)))
        }
    })

    const basicSchema = new Schema({
        implicit:[ZenAmountYamlType,UInt64YamlType,Int64YamlType],
        explicit:[
            StringYamlType,
            DictionaryYamlType
        ],
        include:[]
    })

    function createSchema(chain:Chain) {
        const AddressYamlType = new Type('!address', {
            kind:'scalar',
            resolve: function(data) {
                if (typeof data === 'string') {
                    try {
                        Address.decode(chain, data)
                        return true
                    }
                    catch (ex) {
                        return false
                    }
                }

                return false
            },
            construct:function(data) {
                const lock = Address.decode(chain, data)

                return new PKAddress(lock.pkHash.bytes)
            },
            instanceOf: PKAddress,
            represent: function(pkLock:any) {

                 return Address.getPublicKeyHashAddress(chain, pkLock.hash)
            }
        })

        return new Schema({
            include: [basicSchema],
            implicit: [AddressYamlType]
        })
    }

    export function fromYaml(chain:Chain, yaml:string) {

        //console.log(createSchema(chain))

        const data = load(yaml, {schema: createSchema(chain)})

        // replacing object and strings
        function visit(node:any) : any {
            if (isPlainObject(node)) {
                const pairs = toPairs<any>(node).map<[string,any]>(pair => {
                    const key:string = pair[0]
                    const value:any = visit(pair[1])

                    return [key,value]
                })

                return new Dictionary(pairs)
            }
            else if (typeof node === 'string') {
                return new String(node)
            }

            return node
        }

        return visit(data)
    }

    export function toYaml(chain:Chain, data:any) {
        return dump(data, {schema:createSchema(chain)})
    }

    export function serialize(data:any) {
        const buffer = Buffer.alloc(data.getSize())
        data.write(buffer, 0)

        return buffer.toString('hex')
    }
}

export const fromYaml = Data.fromYaml
export const serialize = Data.serialize
export const toYaml = Data.toYaml

export default Data