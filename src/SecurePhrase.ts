import randomBytes = require('randombytes')
import aesjs = require('aes-js')
import {pbkdf2Sync} from 'pbkdf2'
import createHmac = require('create-hmac')
import {pad,unpad} from 'pkcs7'

// Port of https://gitlab.com/zenprotocol/zenprotocol/blob/master/src/Infrastructure/Security.fs

const SaltSize = 16
const KeySize = 32
const AuthKeySize = 16
const IVSize = 16
const MacSize = 32

function derive(password: string, salt: Buffer) {
    const derived = pbkdf2Sync(password,salt, 1000, KeySize + AuthKeySize + IVSize)

    const key = derived.slice(0,KeySize)
    const authKey = derived.slice(KeySize, KeySize + AuthKeySize)
    const iv = derived.slice(KeySize + AuthKeySize, KeySize + AuthKeySize + IVSize)

    return [authKey, iv,key]
}

export namespace SecurePhrase {
    export function encrypt(password:string, msg:string, encoding:string='utf8') {
        let plain = Buffer.from(msg, encoding)

        const salt = randomBytes(SaltSize)

        const [authKey, iv, key] = derive(password, salt)

        plain = pad(plain)
        const encryptor = new aesjs.ModeOfOperation.cbc(key,iv)
        const cipher = Buffer.from(encryptor.encrypt(plain))

        const hmac = createHmac('sha256', authKey)
        hmac.update(cipher)
        const hash = hmac.digest()

        return Buffer.concat([salt, hash, cipher]).toString('hex')
    }
    
    export function decrypt(password:string, encrypted:string, encoding:string='utf8') {
        const data = Buffer.from(encrypted, 'hex')
        const salt = data.slice(0, SaltSize)
        const hash = data.slice(SaltSize, SaltSize + MacSize)
        const cipher = data.slice(SaltSize + MacSize)

        const [authKey, iv, key] = derive(password, salt)

        const hmac = createHmac('sha256', authKey)
        hmac.update(cipher)

        const hash2 = hmac.digest()

        if (!hash2.equals(hash)) {
            throw new Error('bad password')
        } else {
            const encryptor = new aesjs.ModeOfOperation.cbc(key,iv)
            const plain = Buffer.from(encryptor.decrypt(cipher))

            return unpad(plain).toString(encoding)
        }
    }
}

export default SecurePhrase