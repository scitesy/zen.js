import {Buffer} from 'buffer'
import BigInteger = require('bigi')

const TEN = BigInteger.valueOf(10)
const ZERO = BigInteger.valueOf(0)

export function getSizeOfVarInt(value:number) {
    let size = 0
    while(true) {
        size++
        if (value <= 0x7F)
            break;
        value = (value >> 7) - 1
    }
    return size
}

export function writeVarInt (value:number, buffer:Buffer, offset:number) {
    let tmp = new Uint8Array(5)
    let len=0;

    while(true){
        tmp[len] = (value & 0x7F) | (len !== 0 ? 0x80 : 0x00)
        if (value <= 0x7F)
            break

        value = (value >> 7) - 1
        len++
    }

    tmp = tmp.slice(0,len+1)
    tmp.reverse()

    Buffer.from(tmp).copy(buffer,offset)

    return offset+len+1
}

export function readVarInt (buffer:Buffer, offset: number) {
    let value = 0
    while(true) {
        let chData = buffer.readUInt8(offset)
        offset++

        value = (value << 7) | (chData & 0x7F)
        if ((chData & 0x80) !== 0)
            value++
        else
            return {value, offset}
    }
}

function factor(x:BigInteger) : [BigInteger,number] {
    function inner(s:BigInteger,e:number) : [BigInteger,number] {
        if (!s.mod(TEN).equals(ZERO))
            return [s,e]
        else
            return inner(s.divide(TEN),e+1)
    }

    return inner(x,0)
}

function figures(s:BigInteger) {
    function inner(s:BigInteger,f:number) :number {
        if (s.equals(ZERO))
            return f
        else
            return inner(s.divide(TEN),f+1)
    }

    return inner(s,0)
}

function parse(x:BigInteger) : [BigInteger,number,number] {
    if (x.equals(ZERO))
        return [ZERO,0,0]
    else {
        const [s, e] = factor(x)
        const f = figures(s)

        return [s,e,f]
    }
}

function write16(buffer:Buffer,offset:number,s:BigInteger,e:number) {
    const res = s.or(BigInteger.valueOf(e << 10)).intValue()

    buffer.writeUInt16BE(res, offset)

    return offset + 2
}

const cutoff = BigInteger.valueOf(0x04000000)

function write32(buffer:Buffer,offset:number,s:BigInteger,e:number) {
    if (s.compareTo(cutoff) < 0) {
        const shifted = BigInteger.valueOf(0x20 | e).shiftLeft(26)

        const res = s.or(shifted)

        res.toBuffer(4).copy(buffer, offset)
    } else {
        const shifted = BigInteger.valueOf(0x60 | e).shiftLeft(25)
        const res = s.subtract(cutoff).or(shifted)

        res.toBuffer(4).copy(buffer, offset)
    }

    return offset + 4
}

function write64(buffer:Buffer, offset:number,s:BigInteger) {
    const res = BigInteger.valueOf(0x7E).shiftLeft(56).or(s)

    res.toBuffer(8).copy(buffer, offset)

    return offset + 8
}

function write72(buffer:Buffer, offset:number,s:BigInteger) {
    offset = buffer.writeUInt8(0xFE,offset)
    s.toBuffer(8).copy(buffer, offset)

    return offset + 8
}

const maximum64bit = BigInteger.valueOf(2).pow(BigInteger.valueOf(56))

export function getSizeOfAmount(amount:BigInteger) {
    const [s,e,f] = parse(amount)

    if (f <= 3) {
        return 2
    } else if (f<= 8) {
        return 4
    }
    else if (amount.compareTo(maximum64bit) < 0) {
        return 8
    }
    else {
        return 9
    }
}

export function writeAmount(amount:BigInteger,buffer:Buffer,offset:number) {
    const [s,e,f] = parse(amount)

    if (f <= 3) {
        return write16(buffer, offset, s, e)
    } else if (f<= 8) {
        if (e <= 12) {
            return write32(buffer,offset,s,e)
        } else {
            const m = TEN.pow(BigInteger.valueOf(e-12))

            return write32(buffer,offset, s.multiply(m),12)
        }
    } else if (amount.compareTo(maximum64bit) < 0) {
        return write64(buffer, offset, amount)
    } else {
        return write72(buffer, offset, amount)
    }
}

export function readAmount(buffer:Buffer,offset:number) {
    const first = buffer.readUInt8(offset)
    offset++
    let amount:BigInteger

    if ((first & 0x7E) === 0x7C || (first & 0x7C) === 0x78) {
        throw "invalid amount"
    } else if ((first & 0x7E) === 0x7E) {
        if (first >= 0x80) {
            amount = BigInteger.fromBuffer(buffer.slice(offset, offset+8))
            offset+=8
        }
        else {
            amount = BigInteger.fromBuffer(buffer.slice(offset, offset+7))
            offset+=7
        }
    } else if (first  < 0x80) {
        const second = buffer.readUInt8(offset)
        offset++

        if((first & 0x60) === 0x60){
            const e = first & 0x1F
            const s = second + 0x400
            amount = BigInteger.valueOf(s * Math.pow(10,e))
        } else {
            const e = (first & 0x7C) >>> 2
            const s = second + ((first & 0x03) << 8)
            amount = BigInteger.valueOf(s * Math.pow(10,e))
        }
    } else {
        const second = buffer.readUInt8(offset)
        offset++
        const lower = buffer.readUInt16BE(offset)
        offset+=2

        if ((first & 0x40) === 0) {
            const e = (first & 0x3C) >> 2
            const s = lower + (second << 16) + ((first & 0x03) << 24)
            amount = BigInteger.valueOf(s * Math.pow(10,e))
        } else {
            const e = (first & 0x1E) >> 1
            const s = lower + (second << 16) + ((first & 0x01) << 24) + 0x4000000

            amount = BigInteger.valueOf(s * Math.pow(10,e))
        }
    }

    return {amount:amount,offset:offset}
}