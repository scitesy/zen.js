import {Asset, Hash, Input, Output, PKWitness, Spend, Lock, Chain, SigHashTxHash} from './Types'
import {Transaction} from './Transaction'
import Address from './Address'
import {PrivateKey,Signature} from './Crypto'
import BigInteger = require('bigi')

interface InputWithKey {
    input:Input
    privateKey?:PrivateKey
}

export class TransactionBuilder {
    private inputs:Array<InputWithKey>
    outputs:Array<Output>

    constructor(public chain:Chain) {
        this.inputs = []
        this.outputs = []
    }

    addInput(txHash:string|Hash, index:number, privateKey?:PrivateKey) {
        if (typeof txHash === 'string') {
            txHash = new Hash(txHash)
        }

        this.inputs.push({
            privateKey:privateKey,
            input: new Input({
                    kind:'outpoint',
                    txHash:txHash,
                    index:index
                })
        })
    }

    addOutput(address:string|Lock, amount:number|BigInteger, asset:Asset|string) {
        if (typeof address === 'string')
            address = Address.decode(this.chain,address)

        if (typeof asset === 'string')
            asset = new Asset(asset)

        if (typeof amount === 'number')
            amount = new BigInteger(amount.toString(10),10,undefined)

        const output = new Output(address,new Spend(asset,amount))
        this.outputs.push(output)
    }

    hash() {
        const inputs = this.inputs.map(input => input.input)

        const tx = new Transaction(0, inputs, this.outputs)
        return tx.hash()
    }

    getUnsignedTransaction() {
        const inputs = this.inputs.map(input => input.input)

        return new Transaction(0, inputs, this.outputs)
    }

    sign() {
        const inputs = this.inputs.map(input => input.input)

        const tx = new Transaction(0, inputs, this.outputs)
        const hash = tx.hash().bytes

        tx.witnesses = this.inputs.map(input => {
            if (!input.privateKey)
                throw 'mising private key'

            const signature = input.privateKey.sign(hash)

            const signarueBuffer = signature.serialize()
            const pulicKeyBuffer = input.privateKey.getPublicKey().serialize()

            return new PKWitness(SigHashTxHash, pulicKeyBuffer, signarueBuffer)
        })

        return tx
    }
}