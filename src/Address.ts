import * as bech32 from 'bech32'
import {Hash,PKLock, Lock, Chain} from './Types'

export namespace Address {

    export function getChainType(chain: Chain) {
        switch (chain) {
            case 'local':
                return 'tzn'
            case 'test':
                return 'tzn'
            case 'main':
                return 'zen'
        }
    }

    export function decode(chain: Chain, address: string) {
        const {prefix, words} = bech32.decode(address)

        if (words[0] !== 0)
            throw 'invalid version'

        const data = Buffer.from(bech32.fromWords(words.slice(1)))

        if (prefix === getChainType(chain)) {
            const {hash: pkHash} = Hash.read(data, 0)
            return new PKLock(pkHash)
        } else if (prefix === 'c' + getChainType(chain)) {
            throw 'contract addresses not yet supported'
        } else {
            throw 'not supported'
        }
    }

    export function getPublicKeyHashAddress(chain:Chain, pkHash:Buffer | string | Hash) {
        if (typeof pkHash === 'string') {
            pkHash = Buffer.from(pkHash,'hex')
        }
        else if (pkHash instanceof Hash) {
            pkHash = pkHash.bytes
        }

        const words = bech32.toWords(pkHash)

        const withVersion = Buffer.alloc(words.length + 1)
        withVersion.writeInt8(0,0)
        Buffer.from(words).copy(withVersion,1)

        const hrp = Address.getChainType(chain)

        return bech32.encode(hrp, withVersion)
    }
}

export default Address